package model

type TodoRequest struct {
	TaskName string `json:"name"`
	TaskDesc string `json:"description"`
	UserId uint64
}

type TodoResponse struct {
	TaskName string `json:"name"`
	TaskDesc string `json:"description"`
	Username string `json:"creator"`
}
