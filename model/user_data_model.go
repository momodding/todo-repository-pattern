package model

import dataType "go-todos-clean/helper"

type ProfileRequest struct {
	Fullname    string `json:"fullname"`
	Address     string `json:"address"`
	DateOfBirth dataType.JSONDate `json:"dateOfBirth"`
	Occupation  string `json:"occupation"`
	Phonenumber string `json:"phonenumber"`
	Socmed string `json:"socmed"`
	Experience string `json:"experience"`
}
