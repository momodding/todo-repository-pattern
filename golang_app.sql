-- -------------------------------------------------------------
-- TablePlus 3.12.2(358)
--
-- https://tableplus.com/
--
-- Database: golang_app
-- Generation Time: 2021-02-11 19:45:53.1960
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `master_role`;
CREATE TABLE `master_role` (
  `mr_r_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_r_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `mr_r_description` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `mr_r_authority` int(11) DEFAULT NULL,
  `mr_r_created_at` datetime DEFAULT current_timestamp(),
  `mr_r_updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `mr_r_deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`mr_r_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `user_credential`;
CREATE TABLE `user_credential` (
  `uc_id` int(11) NOT NULL AUTO_INCREMENT,
  `uc_username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `uc_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `uc_password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `uc_status` enum('Y','N') CHARACTER SET utf8 DEFAULT NULL COMMENT 'Y/N',
  `uc_role` tinyint(1) DEFAULT NULL,
  `uc_login_attempt` int(11) DEFAULT 0,
  `uc_created_at` datetime DEFAULT current_timestamp(),
  `uc_updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`uc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `user_data`;
CREATE TABLE `user_data` (
  `ud_id` int(11) NOT NULL AUTO_INCREMENT,
  `ud_uc_id` int(11) DEFAULT NULL,
  `ud_fullname` varchar(25) DEFAULT NULL,
  `ud_address` varchar(100) DEFAULT NULL,
  `ud_dob` date DEFAULT NULL,
  `ud_occupation` varchar(25) DEFAULT NULL,
  `ud_phone` varchar(15) DEFAULT NULL,
  `ud_socmed` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `ud_experience` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `ud_created_at` datetime DEFAULT current_timestamp(),
  `ud_updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`ud_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `user_todo`;
CREATE TABLE `user_todo` (
  `ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `ut_task_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ut_task_description` text CHARACTER SET latin1 DEFAULT NULL,
  `ut_created_at` datetime DEFAULT current_timestamp(),
  `ut_updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `ut_deleted_at` datetime DEFAULT NULL,
  `ut_uc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `ut_uc_id` int(11) NOT NULL,
  `ut_token` varchar(255) NOT NULL,
  `ut_expire_in` datetime NOT NULL,
  `ut_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=utf8mb4;

INSERT INTO `master_role` (`mr_r_id`, `mr_r_name`, `mr_r_description`, `mr_r_authority`, `mr_r_created_at`, `mr_r_updated_at`, `mr_r_deleted_at`) VALUES
('1', 'subscriber', 'user who subscribe our services', '2', '2020-01-05 07:56:43', '2020-01-05 07:56:47', NULL),
('2', 'admin', 'user who have administration right', '1', '2020-01-05 07:57:17', '2020-01-05 07:57:19', NULL);

INSERT INTO `user_credential` (`uc_id`, `uc_username`, `uc_email`, `uc_password`, `uc_status`, `uc_role`, `uc_login_attempt`, `uc_created_at`, `uc_updated_at`) VALUES
('2', 'Borders', 'Anne.Beatty@yahoo.com', 'S7gtlaMrxlSSnFK', 'Y', '1', '0', NULL, NULL),
('3', 'Exclusive', 'Gregg55@gmail.com', 'LnHPRJG9Q7cg5Hy', 'Y', '1', '0', NULL, NULL),
('4', 'Gloves', 'Lane61@yahoo.com', '6x2W5R22vWhgbFX', 'Y', '1', '0', '2020-01-05 09:10:07', NULL),
('5', 'Designer', 'Colton91@yahoo.com', 'fwAZmVrcjF1NVp9', 'Y', '1', '0', '2020-01-05 09:26:26', NULL),
('6', 'Engineer', 'engineer@mail.com', 'asd', 'Y', '1', '0', '2020-02-22 12:52:19', '2020-02-22 05:52:23'),
('7', 'Test', 'test@mail.com', 'asd', 'Y', '2', '0', '2020-02-22 16:45:54', '2020-06-27 07:27:32'),
('11', 'Test123', 'test2@mail.com', 'asd', 'Y', '2', '0', '2020-06-28 01:23:02', '2020-06-28 01:23:02'),
('21', 'loremipsum', 'test3@mail.com', 'asd', 'Y', '2', '0', '2020-12-25 21:38:56', '2020-12-25 21:38:56');

INSERT INTO `user_data` (`ud_id`, `ud_uc_id`, `ud_fullname`, `ud_address`, `ud_dob`, `ud_occupation`, `ud_phone`, `ud_socmed`, `ud_experience`, `ud_created_at`, `ud_updated_at`) VALUES
('1', '7', 'Utsman Fajar', 'Jl Flamboyan Panjer', '2020-07-26', 'Software Engineer', '089611228978', '[{\"name\":\"linkedin\",\"type\":\"socmed\",\"link\":\"https://www.linkedin.com/in/utsman-fajar-b01a63169/\",\"desc\":\"Utsman Fajar\"}]', '', '2020-12-27 20:32:19', '2020-12-28 10:29:58');

INSERT INTO `user_todo` (`ut_id`, `ut_task_name`, `ut_task_description`, `ut_created_at`, `ut_updated_at`, `ut_deleted_at`, `ut_uc_id`) VALUES
('91', 'dfsdf', 'Brandal17', '2020-05-06 09:29:14', '2020-05-06 09:29:14', '2020-06-25 06:49:21', '6'),
('101', 'bodo amat', 'dableg', '2020-05-07 12:52:49', '2020-05-07 12:55:11', '2020-05-07 12:55:11', NULL),
('151', 'bodo amat', 'badjingan', '2020-05-08 07:34:06', '2020-05-08 07:34:06', '2020-06-25 06:48:49', '6'),
('161', 'asdsad', 'sd', '2020-06-24 09:34:36', '2020-06-24 10:58:17', '2020-06-24 10:58:17', '7'),
('171', 'asd', 'dasd', '2020-06-24 09:36:41', '2020-06-24 10:58:15', '2020-06-24 10:58:15', '7'),
('181', 'asd', 'sad', '2020-06-24 09:53:21', '2020-06-24 10:58:13', '2020-06-24 10:58:13', '7'),
('191', 'asd', 'fsdfsdf', '2020-06-24 10:39:41', '2020-06-24 10:58:11', '2020-06-24 10:58:11', '7'),
('201', 'fds', 'sdfsd', '2020-06-24 10:58:49', '2020-06-24 11:18:36', '2020-06-24 11:18:36', '7'),
('211', 'fdsf', 'sdfddf', '2020-06-24 10:59:12', '2020-06-24 11:18:28', '2020-06-24 11:18:28', '7'),
('221', 'fsdfsd', 'sdfsdf', '2020-06-25 06:52:05', '2020-06-25 06:52:05', NULL, '7'),
('231', 'fsdfs', 'sdfsdfsdfsd', '2020-06-27 09:21:51', '2020-06-27 09:21:51', NULL, '7'),
('241', '', '', '2020-10-28 07:11:00', '2020-10-28 07:11:00', '2020-10-28 07:14:42', '0'),
('251', 'fajars', 'fajar sayang septi', '2020-10-28 07:12:00', '2020-10-28 09:13:31', NULL, '0'),
('271', 'fajars', 'fajar sayang septi', '2020-10-28 07:56:46', '2020-12-26 14:54:27', '2020-12-26 21:54:27', '0'),
('283', 'bodoamat', 'asdcasdasd', '2020-12-25 20:32:48', '2020-12-25 13:33:05', '2020-12-25 20:33:05', '7'),
('284', 'bodoamat', 'Curabitur suscipit suscipit tellus', '2020-12-25 20:34:24', '2020-12-25 14:11:29', '2020-12-25 21:11:29', '7'),
('285', 'bodoamat', 'hytnbtgvdfv', '2020-12-26 21:50:13', '2020-12-26 14:54:08', '2020-12-26 21:54:08', '7'),
('286', 'bodoamat', 'Nulla sit amet est', '2020-12-27 16:33:21', '2020-12-27 16:33:21', NULL, '7');

INSERT INTO `user_token` (`ut_id`, `ut_uc_id`, `ut_token`, `ut_expire_in`, `ut_created_at`) VALUES
('1', '7', '$2a$10$iU58CQEkVbbkLdWDvfimjezf.FzoQIqgH/R9gIm/6HhEgfy.zygyK', '2020-06-27 21:54:37', '2020-06-27 13:54:37'),
('11', '7', '$2a$10$lMjkr.aDrtcx9WwVmiKw7ONM.eiVJJP5wrxz/tTzhgbUE3UMH2cAu', '2020-06-27 21:56:12', '2020-06-27 13:56:13'),
('21', '7', '$2a$10$OPkveZdhGoGBoL5wG7nmaupAP62SOW9PH9tTUdM1lmeEpTYnQeLPq', '2020-06-26 21:56:16', '2020-06-27 13:56:16'),
('31', '7', '$2a$10$ZboCyvxB74zMgguRc19KjurYKGzBA2KnbtHhEzlDylqwBoMhdEX0m', '2020-06-27 22:03:03', '2020-06-27 14:03:03'),
('41', '7', '$2a$10$hu4j.5ctEOyG68/eA3YipuaGnSIYEcAJo2783/85MPuyETS5x/2HS', '2020-06-27 15:27:32', '2020-06-27 07:27:32'),
('51', '7', '$2a$10$Hf4BTPzS2Ib77VpRehaq/e7.kjkAthkLEia3MT.k1.oEaGg246OpC', '2020-06-27 15:35:35', '2020-06-27 07:35:35'),
('61', '7', '$2a$10$tNevn1xmcd9pANc.0uXhT.shQ1oYhMr8tEIHjQ5bEpClDfyO8tKKO', '2020-06-27 15:47:18', '2020-06-27 07:47:18'),
('71', '7', '$2a$10$3.GKihMIy5/SL4CVKW5C1.c00WiJ4jB.8ORGODVXvOE7WY/AvdQtG', '2020-06-27 15:50:23', '2020-06-27 07:50:23'),
('81', '7', '$2a$10$aTJFH/zqF..HbJFb82lnm.V8wlKMcYhH/xT30tVxcokS752QmiSVK', '2020-06-26 17:07:41', '2020-06-27 09:07:41'),
('91', '7', '$2a$10$MRPK99T89pMPYQnXI8eR/OVsg62tzgoI49eMWNVHFpfi9J74pgBHu', '2020-06-27 17:20:03', '2020-06-27 09:20:03'),
('101', '7', '$2a$10$6tK5TfRE1wFm1vgjSDvEa.VAc4HX/01kadRcCDHe8Bp74r2YjL6YC', '2020-06-27 17:23:39', '2020-06-27 09:23:39'),
('111', '7', '$2a$10$TYXuuuiT/TK1RLUNnbrcNe6/zVhF8sfXGFSxzMvQDmstbqF.qsBtm', '2020-06-27 18:32:13', '2020-06-27 10:32:13'),
('121', '11', '$2a$10$v4M57xCCjN8G.6idW2hJSu7xu2gdT1mNNc0mqJW/nnp5LG5md/tSa', '2020-06-28 09:23:03', '2020-06-28 01:23:03'),
('131', '7', '$2a$10$LC/7nM6jSJ2KyXMQsiRt/.c77P5vFqYJj9hGHysRykXwKQymoTSkW', '2020-06-28 18:38:31', '2020-06-28 10:38:31'),
('141', '7', '$2a$10$FZbPnKNfrrT7qhk8RR7OPuXGqx3niev8MxmD5tuXxWkP67.9qbpb6', '2020-07-09 14:35:29', '2020-07-09 06:35:29'),
('151', '7', '$2a$10$OsTHRwklj.anu1VjsQqqEulNIEqxSKFALMb/Vvaae6r6ScLGxgjq.', '2020-07-09 15:41:04', '2020-07-09 07:41:04'),
('161', '7', '$2a$10$fe/KAaPJNbuxdMUUyBlHheoVsaUJTQvxt1I0p5N1Eq1dd7ocMAEsa', '2020-07-09 16:32:23', '2020-07-09 08:32:23'),
('171', '7', '$2a$10$/GhbaF0CSlZ/TXEigJUebO0Ha8c1ZhA8dNm2/cvLn8bo68x5bx2Qa', '2020-07-09 16:47:33', '2020-07-09 08:47:33'),
('181', '7', '$2a$10$YI/DUqS5LOmqAZfiHjloue1CuSD3RFgBStknguwyaWj7kvVEKkemO', '2020-07-09 17:22:48', '2020-07-09 09:22:48'),
('191', '7', '$2a$10$sSk4ttjB4yRgEdL54MO6oeNwnLRHj7gtyZ31HZSR5.742tlOAfu82', '2020-07-09 17:42:04', '2020-07-09 09:42:04'),
('201', '7', '$2a$10$9vc3Sbvshu4BT5PChlLU4O348CQ5zvbDxydx9JtCTEEc4An84wdYq', '2020-07-09 18:03:47', '2020-07-09 10:03:47'),
('211', '7', '$2a$10$gNT92hMTbMCyEEDG0Va7we9jEnWqd8HXswZjbLLG4QfIhd2lM6p6O', '2020-07-09 18:29:21', '2020-07-09 10:29:21'),
('221', '7', '$2a$10$GdhVQIxUQPfCYG09Q9clGOlcFX1JRFj.oEcJ1Yeafy9yQxA64OupC', '2020-07-09 21:40:02', '2020-07-09 13:40:02'),
('231', '7', '$2a$10$36fcw6ZuPpzd4Iku8/aB2u9HIYGJ/6XUS.Rp1M4g2WDYAjvINIMoq', '2020-07-10 07:54:36', '2020-07-09 23:54:36'),
('241', '7', '$2a$10$sfgEWlia17RCwUGQRzvgx.dw6Aj33M2/uYfMnyscDwucdug.te6Jq', '2020-07-10 12:24:07', '2020-07-10 04:24:07'),
('251', '7', '$2a$10$7KJE6Afh441HMGMfngmujen9PHJZTPfDyh4krxhs62PDsKMmLwZP.', '2020-07-10 12:37:05', '2020-07-10 04:37:05'),
('261', '7', '$2a$10$A7Ss9qFAnFRhbCkcivbg4OrHUFhPj6GXBkpJwQoAzktZbnjHFzh8W', '2020-07-10 12:39:05', '2020-07-10 04:39:05'),
('271', '7', '$2a$10$2EZLt/dlrb92b1QHpjIWheLqWZbcWnQlLhFl0Yfvs3hYnzn5fMoiO', '2020-07-10 13:57:33', '2020-07-10 05:57:33'),
('281', '7', '$2a$10$Ex5LMIdKA6lDWdlf.aDuTe10lriH/1j7FMAc3QZWW9yvuX65wnLrW', '2020-07-18 18:59:53', '2020-07-18 10:59:53'),
('291', '7', '$2a$10$zV/xiZJBsSjx06NmMz0xfua8Vy2Czxsf8pYvhsmzlU4luxsZWoBJm', '2020-07-18 19:00:18', '2020-07-18 11:00:18'),
('301', '7', '$2a$10$li8.9qjZ8aQ4xh4OOpkGFemJy7Qz7hV/5J.omiF7bk0rXL6oPfA9m', '2020-07-18 19:00:27', '2020-07-18 11:00:27'),
('311', '7', '$2a$10$q3kDK94/Ie3BP0kKyGkrXuQ5p.jvZPSkcdROcDV7TfIAQ8va3vEhK', '2020-07-18 19:00:50', '2020-07-18 11:00:50'),
('321', '7', '$2a$10$DVdmwtTH4in3otHvUHqVzu2pkjFrqTpFa0kEIMajIa05DI7lQytg2', '2020-07-18 19:01:32', '2020-07-18 11:01:32'),
('331', '7', '$2a$10$BnZbtSL0oUC7U5YrkkfpVuIBpJXP5pzpRafDZDzBQ3d4TxnALEgb.', '2020-07-21 15:02:14', '2020-07-21 07:02:14'),
('341', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1MDYxOTYsInJlZnJlc2hfdXVpZCI6ImQyNDFiMTc4LWFkZWMtNDBjNS05NWY4LTcwYzllNWE1ZmJlMSsrNyIsInVzZXJfaWQiOjd9.I10MzSb1Qvk3mdUxwuDtKdLiZRkr7ih6CFGyYirIRr4', '2020-12-26 04:03:16', '2020-12-25 20:03:16'),
('342', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1MDcyNTcsInJlZnJlc2hfdXVpZCI6IjYxMWM5NzcxLWE5NmMtNGEwZS05OTMxLTEzNTY0MTJhOTU4OSsrNyIsInVzZXJfaWQiOjd9.xSn574hEbiOB4urV8sAfvTc3IW3qOefq9uROStWgh90', '2020-12-26 04:20:57', '2020-12-25 20:20:57'),
('343', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1MDc3NTEsInJlZnJlc2hfdXVpZCI6IjkwYjc0ZWYxLTJkMDMtNDYxNy05YzkxLTFmZWQ0ZTFkZTI3NCsrNyIsInVzZXJfaWQiOjd9.lUpOisa0Ji--kFYp0jT2udkwrx7P101yj1QYD2JDpbQ', '2020-12-26 04:29:11', '2020-12-25 20:29:11'),
('344', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1MTAyNDYsInJlZnJlc2hfdXVpZCI6IjU5NmI2NzRlLTJmMjYtNGZhYS05Y2I3LWJjNGJmZDU4ODkyNCsrNyIsInVzZXJfaWQiOjd9.uKBB6sg27o1t2t5qUXYWT1KQBnUmmXYZhw-RLDJwvbo', '2020-12-26 05:10:46', '2020-12-25 21:10:46'),
('345', '21', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1MTE5MzYsInJlZnJlc2hfdXVpZCI6IjA4NDA2NDU2LTNhZjgtNDE4OC1iMzc4LTM4NTViOWU2MTM3NCsrMjEiLCJ1c2VyX2lkIjoyMX0.KJjhg0InaHT4Z9kC6g85wrdMg-HSzeIwhqG7d8eQLk4', '2020-12-26 05:38:56', '2020-12-25 21:38:56'),
('346', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1MTIwMjMsInJlZnJlc2hfdXVpZCI6ImQxNTI1YTNlLTQ2M2MtNGY0OC05MGEzLTgwNjUwN2RhNzgyZSsrNyIsInVzZXJfaWQiOjd9.3sgtEdVeSxkKALMKmLusiAEd05y6KX4mQfiMHUlrcEU', '2020-12-26 05:40:23', '2020-12-25 21:40:23'),
('347', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjE4NDgsInJlZnJlc2hfdXVpZCI6IjRiY2NhMjdiLWM0ODktNDNiNC05MGEzLWZmZDkwODJmNzYxOSsrNyIsInVzZXJfaWQiOjd9.dK0pd9yAcGaBu9SGdOX4bapxM93xN9z_nz1u5SARWi0', '2020-12-26 19:30:48', '2020-12-26 11:30:48'),
('348', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjIwNDQsInJlZnJlc2hfdXVpZCI6ImQ5ODAzNjM3LWU3YjgtNDcyZC05MGJiLTg5YmQxOGZkNDUwMSsrNyIsInVzZXJfaWQiOjd9.d2cWiOIkeJ_cJR3o2wL3jd9vRCyHEMypU8TandQkGDU', '2020-12-26 19:34:04', '2020-12-26 11:34:04'),
('349', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjIyOTcsInJlZnJlc2hfdXVpZCI6IjAzYTMzNDQzLTAwOTMtNGE4Ny1hNTE4LTYyMzdhYjNmMmMzMCsrNyIsInVzZXJfaWQiOjd9.85cRK-66A7PrCJf2zAAkwfnTKK4g5hPs-RjabJ9ap9s', '2020-12-26 19:38:17', '2020-12-26 11:38:17'),
('350', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjMyOTEsInJlZnJlc2hfdXVpZCI6IjMzMDVmNDM4LWZlM2ItNGM3Zi1iYWJjLTQ3YTdlZTA0ZGIyOSsrNyIsInVzZXJfaWQiOjd9.yuqMdIsEILeRAuOkj-PepXRmCVpiqA8d1GpwZXZLqH4', '2020-12-26 19:54:51', '2020-12-26 11:54:51'),
('351', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjM4MjUsInJlZnJlc2hfdXVpZCI6ImU2ZDgwN2U0LTdmZTUtNDU1Yi1iOGNmLWIwZWU4YjdmOTQ1NysrNyIsInVzZXJfaWQiOjd9.QjfILuABdFSyq77IUC2QmFnF6rtFtoFu7lnB1oyj9tM', '2020-12-26 20:03:45', '2020-12-26 12:03:45'),
('352', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjUzODMsInJlZnJlc2hfdXVpZCI6IjJkNjZkM2QxLTM2NzEtNDNkYS1iMzA2LTdjZmI1ODZlNTZjNysrNyIsInVzZXJfaWQiOjd9.Lbm1A4cTWnAstXG9wFvQBfF1tGSlI-JoeuI_hrbhVEc', '2020-12-26 20:29:43', '2020-12-26 12:29:43'),
('353', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjU1NTksInJlZnJlc2hfdXVpZCI6IjQ3NTYyMzVkLThlNTUtNGQwZS05NDc1LTJjOThmNzJkZjhmOSsrNyIsInVzZXJfaWQiOjd9.fQ-amwQ5yMVqBkybbj1eGQ5G6eF-WkxjTHum2czOeSg', '2020-12-26 20:32:39', '2020-12-26 12:32:39'),
('354', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjU3MjksInJlZnJlc2hfdXVpZCI6IjdiZDFiNTRlLWUxYWUtNDJkMi05MDQzLWQ2MDRkZDgwYjkxOCsrNyIsInVzZXJfaWQiOjd9.cwDob5ewXgN01SwoX-hjtCrsgKcL9s1xLVCxMTYB4XM', '2020-12-26 20:35:29', '2020-12-26 12:35:29'),
('355', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjU4NzYsInJlZnJlc2hfdXVpZCI6IjM3YTM1NzU3LTUzNjQtNGQ5NS05MWViLTJkYzU1NTg3NWVmOSsrNyIsInVzZXJfaWQiOjd9.P5IOElfcuNAMAxwEJNMnO7bheHRFpCEwDHtcfgkFU-4', '2020-12-26 20:37:56', '2020-12-26 12:37:56'),
('356', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjYwMTAsInJlZnJlc2hfdXVpZCI6IjVmNDZiNmE0LTcxMDktNDQ0Yy1hN2QzLWIwODc3ZGY5NWIyOCsrNyIsInVzZXJfaWQiOjd9.6SYpb3aLK7zhGvRMrChaalvzpi5cZ-mA0tp4TYmElgQ', '2020-12-26 20:40:10', '2020-12-26 12:40:10'),
('357', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjY2NDEsInJlZnJlc2hfdXVpZCI6IjM5MjBhNDFiLWU0NTEtNDcwNS04MTk2LWUwZjQ4NzM3ZjMwYysrNyIsInVzZXJfaWQiOjd9.oSXDpH4_Q6-T4jvgeaqjJdhGVGrAoDPtIRqBC1KZ39A', '2020-12-26 20:50:41', '2020-12-26 12:50:41'),
('358', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjY5MjgsInJlZnJlc2hfdXVpZCI6Ijk5NzQwMmI1LWM2YjgtNDMzOS05MTcyLWQ3NTVjYWRiMjkxNCsrNyIsInVzZXJfaWQiOjd9.iWmOzUAK7pEpBmcyRyH6CJYjG4RQkoXQVUAkXOeLi8M', '2020-12-26 20:55:28', '2020-12-26 12:55:28'),
('359', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjcwNjgsInJlZnJlc2hfdXVpZCI6Ijg0YjkzY2QxLTAzZTItNGM2Ny1iODMzLWM5ZDMyYjY2OWM3NysrNyIsInVzZXJfaWQiOjd9.aeG4ZR7ae3trrC5otNvQ93AGfV6OFjXudCAMuRya3U8', '2020-12-26 20:57:48', '2020-12-26 12:57:48'),
('360', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1NjcxMjQsInJlZnJlc2hfdXVpZCI6IjFlOTUxYTU0LWQzZGQtNGY1ZS04NzEwLTJkNGI4MGU1NjFkNysrNyIsInVzZXJfaWQiOjd9.MDa8MZIxb9wJ_PMCPDyROWfRiFHyWL6TR94d9sgwY_s', '2020-12-26 20:58:44', '2020-12-26 12:58:44'),
('361', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1Njk0MzUsInJlZnJlc2hfdXVpZCI6Ijg5ZWViNTEwLTZjMWEtNDUwNy1iZmVhLWI1ODAyMzcwMmI2YysrNyIsInVzZXJfaWQiOjd9.7bb4Ujw2dROPsQ5t3aaoK2Yzx3YH2RgthiqzPQOfRa8', '2020-12-26 21:37:15', '2020-12-26 13:37:15'),
('362', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1OTg3NzAsInJlZnJlc2hfdXVpZCI6IjMyOTE5MGExLTcxZTUtNGRlOC1iOTZjLTEyNzcxMmQ0MWFjOSsrNyIsInVzZXJfaWQiOjd9.qol1uUw1lpMIWpMEHIVXTldlVr4ejN1hSThBstKrqbI', '2020-12-27 05:46:10', '2020-12-26 21:46:10'),
('363', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk1OTg4OTEsInJlZnJlc2hfdXVpZCI6IjgxYTY2MTcyLWI1MDUtNDExZC1hODMxLWRiMjdlODE2Y2RlZCsrNyIsInVzZXJfaWQiOjd9.oSFw8ML4Wbd-qfznUq-HsjYMf0gBhNnt22ddpTN9yYM', '2020-12-27 05:48:11', '2020-12-26 21:48:11'),
('364', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk2NjQ2ODAsInJlZnJlc2hfdXVpZCI6ImRhMzkxNTE5LTNmMGItNDdlYi04NDM4LTdmNTNlZWE1ZDAzZisrNyIsInVzZXJfaWQiOjd9.3G4r0elhvKFMgoqCb50f1b8bwgIjx1pGbpGZmFN0VnY', '2020-12-28 00:04:40', '2020-12-27 16:04:40'),
('365', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk2NjYyMDcsInJlZnJlc2hfdXVpZCI6ImNmNmU4YmE0LThlN2QtNDBkOC1hZWRiLTRjYjYyZDE4OWZiYysrNyIsInVzZXJfaWQiOjd9.jXUtY8Rkb0DBAXygRv5OxhM32BDDTgQ1w00Vm_ZWGeo', '2020-12-28 00:30:07', '2020-12-27 16:30:07'),
('366', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk2Njg2NjksInJlZnJlc2hfdXVpZCI6IjZmZWFkNmU0LTNlYWUtNDNhYy1hMzljLTllMmU0MTQ3NzQ1MCsrNyIsInVzZXJfaWQiOjd9.e4e1p0NbqbZXorcyNX6mEfM4aMvm_mtmkC_5dAxSj2A', '2020-12-28 01:11:09', '2020-12-27 17:11:09'),
('367', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk2NzY1ODcsInJlZnJlc2hfdXVpZCI6IjhiZWJkYTc2LTQwMjQtNDVmZC1hYjJhLWU3M2E0N2NlZDk3YisrNyIsInVzZXJfaWQiOjd9.n6dFZpw61DNnQa9YJispPxV9ujB_gzkAqydtCusyyCM', '2020-12-28 03:23:07', '2020-12-27 19:23:07'),
('368', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk3MjY4MzYsInJlZnJlc2hfdXVpZCI6ImRiM2RlYzdlLTBiY2QtNDk1My1hOWVhLTVjOWE4NmRhOTZlZCsrNyIsInVzZXJfaWQiOjd9.MhsTo-cBqhMgBRv-mXkMaX4GmG0keRIkGlSXoFymT8c', '2020-12-28 17:20:36', '2020-12-28 09:20:36'),
('369', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk3MzE5MTcsInJlZnJlc2hfdXVpZCI6IjBjMDk0MGE0LWFjMjEtNGIzNC1hYjkxLTY3NzM0ZmQ1MzFiNSsrNyIsInVzZXJfaWQiOjd9.7omA08wjMiUy1bny2Zo10_eUY5ooDP66KN8xesIqWzQ', '2020-12-28 18:45:17', '2020-12-28 10:45:17'),
('370', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTE0NzQyNzEsInJlZnJlc2hfdXVpZCI6IjliOWJjNzE4LWQ3OGEtNGEzOC1hM2MzLTJiODAwMzk1NTM0YSsrNyIsInVzZXJfaWQiOjd9.S6zJ9nTFCaaApOE1PrftyGTsU4wpGYO8OCyJroWxD8A', '2021-01-17 22:44:31', '2021-01-17 14:44:31'),
('371', '7', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTE0NzQzNTEsInJlZnJlc2hfdXVpZCI6ImNiYTljZjY3LWEwYWEtNDY5OS1hY2Q3LTJiZWYxMzU1ZTA4OCsrNyIsInVzZXJfaWQiOjd9.lw3F4It0zEC87krRhPh0EBs_3V_Gf4FUlLHoHGJwDE4', '2021-01-17 22:45:51', '2021-01-17 14:45:51');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;