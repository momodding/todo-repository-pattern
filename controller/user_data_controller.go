package controller

import (
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/config"
	"go-todos-clean/model"
	"go-todos-clean/service/userdata"
)

type UserDataController struct {
	UserDataService userdata.UserDataService
	Token           config.Token
}

func NewUserDataController(
	userDataService *userdata.UserDataService,
	token *config.Token,
) UserDataController {
	return UserDataController{
		UserDataService: *userDataService,
		Token: *token,
	}
}

func (t *UserDataController) GetProfile(c *fiber.Ctx) error {
	tokenData, err := t.Token.GetUserFromToken(c)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	err, userData := t.UserDataService.GetProfile(tokenData)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, "todo not found")
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code: fiber.StatusOK,
		Data: userData,
		Status: "update success",
	})
}

func (t *UserDataController) CreateOrUpdatePersonalProfile(c *fiber.Ctx) error {
	tokenData, err := t.Token.GetUserFromToken(c)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	var request model.ProfileRequest
	if err := c.BodyParser(&request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	err, userData := t.UserDataService.CreateOrUpdatePersonalProfile(tokenData, request)
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code: fiber.StatusOK,
		Data: userData,
		Status: "update success",
	})
}

func (t *UserDataController) CreateOrUpdateSocmed(c *fiber.Ctx) error {
	tokenData, err := t.Token.GetUserFromToken(c)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	var request model.ProfileRequest
	if err := c.BodyParser(&request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	err, userData := t.UserDataService.CreateOrUpdateSocmed(tokenData, request)
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code: fiber.StatusOK,
		Data: userData,
		Status: "update success",
	})
}

func (t *UserDataController) CreateOrUpdateExperience(c *fiber.Ctx) error {
	tokenData, err := t.Token.GetUserFromToken(c)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	var request model.ProfileRequest
	if err := c.BodyParser(&request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error())
	}

	err, userData := t.UserDataService.CreateOrUpdateExperience(tokenData, request)
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code: fiber.StatusOK,
		Data: userData,
		Status: "update success",
	})
}
