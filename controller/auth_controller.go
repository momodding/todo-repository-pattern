package controller

import (
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/model"
	"go-todos-clean/service/auth"
	"time"
)

type AuthController struct {
	AuthService auth.AuthService
}

func NewAuthController(
	authService *auth.AuthService,
) AuthController {
	return AuthController{ AuthService: *authService}
}

func (t *AuthController) Login(c *fiber.Ctx) error {
	var request model.LoginRequest
	if err := c.BodyParser(&request); err != nil {
		return err
	}

	err, users, token := t.AuthService.Login(request)
	if err != nil {
		return err
	}

	c.Cookie(&fiber.Cookie{
		Name:     "accessToken",
		Value:    token.AccessToken,
		Expires:  time.Now().Add(15 * time.Minute),
		SameSite: "lax",
		Secure:   false,
		HTTPOnly: true,
	})
	return c.Status(fiber.StatusOK).JSON(
		model.WebResponse{
			Code: fiber.StatusOK,
			Data: fiber.Map{
				"accessToken":  token.AccessToken,
				"email":        users.Email,
				"role":         users.Role,
				"refreshToken": token.RefreshToken,
				"username":     users.Username,
			},
			Status: "login sukses",
		},
	)
}

func (t *AuthController) Register(c *fiber.Ctx) error {
	var request model.RegistrationRequest
	if err := c.BodyParser(&request); err != nil {
		return err
	}

	err, users, token := t.AuthService.Register(request)
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(
		model.WebResponse{
			Code: fiber.StatusOK,
			Data: fiber.Map{
				"accessToken":  token.AccessToken,
				"email":        users.Email,
				"role":         users.Role,
				"refreshToken": token.RefreshToken,
				"username":     users.Username,
			},
			Status: "register sukses",
		},
	)
}

func (t *AuthController) RefreshToken(c *fiber.Ctx) error {
	refreshTokenRequest := c.Query("refreshToken")

	err, tokenData, token := t.AuthService.RefreshToken(refreshTokenRequest)
	if err != nil {
		return err
	}

	c.Cookie(&fiber.Cookie{
		Name:     "accessToken",
		Value:    token.AccessToken,
		Expires:  time.Now().Add(15 * time.Minute),
		SameSite: "lax",
		Secure:   false,
		HTTPOnly: true,
	})
	return c.Status(fiber.StatusOK).JSON(
		model.WebResponse{
			Code: fiber.StatusOK,
			Data: fiber.Map{
				"accessToken":  token.AccessToken,
				"refreshToken": token.RefreshToken,
				"expireIn":     tokenData.ExpireIn,
			},
			Status: "refresh token sukses",
		},
	)
}

func (t *AuthController) Logout(c *fiber.Ctx) error {
	c.ClearCookie("accessToken")
	return c.Status(fiber.StatusOK).JSON(model.WebResponse{Code: fiber.StatusOK, Data: true, Status: "logout sukses"})
}
