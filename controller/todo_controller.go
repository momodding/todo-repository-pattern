package controller

import (
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/config"
	"go-todos-clean/model"
	"go-todos-clean/service/todo"
	"strconv"
)

type TodoController struct {
	TodoService todo.TodoService
	Token       config.Token
}

func NewTodoController(
	todoService *todo.TodoService,
	token *config.Token,
) TodoController {
	return TodoController{
		TodoService: *todoService,
		Token:       *token,
	}
}

func (t *TodoController) SaveTodo(c *fiber.Ctx) error {
	var request model.TodoRequest
	if err := c.BodyParser(&request); err != nil {
		return err
	}

	tokenData, err := t.Token.GetUserFromToken(c)
	if err != nil {
		return err
	}
	request.UserId = tokenData.UserId

	err, savedTodos := t.TodoService.SaveTodo(request)
	if err != nil {
		return err
	}
	return c.Status(fiber.StatusCreated).JSON(model.WebResponse{
		Code:   fiber.StatusCreated,
		Data:   savedTodos,
		Status: "create success",
	})
}

func (t *TodoController) UpdateTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return err
	}

	var request model.TodoRequest
	if err := c.BodyParser(&request); err != nil {
		return err
	}

	tokenData, err := t.Token.GetUserFromToken(c)
	if err != nil {
		return err
	}
	request.UserId = tokenData.UserId

	err, updateTodos := t.TodoService.UpdateTodo(todoId, request)
	if err != nil {
		return err
	}
	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:   fiber.StatusOK,
		Data:   updateTodos,
		Status: "update success",
	})
}

func (t *TodoController) DeleteTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return err
	}

	err = t.TodoService.DeleteTodo(todoId)
	if err != nil {
		return err
	}
	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:   fiber.StatusOK,
		Data:   true,
		Status: "delete success",
	})
}

func (t *TodoController) GetTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return err
	}

	err, todos := t.TodoService.GetTodo(todoId)
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:   fiber.StatusOK,
		Data:   todos,
		Status: "fetch success",
	})
}

func (t *TodoController) GetMappedTodo(c *fiber.Ctx) error {
	todoId, err := strconv.ParseUint(c.Params("todo_id"), 10, 64)
	if err != nil {
		return err
	}

	err, todos := t.TodoService.GetMappedTodo(todoId)
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:   fiber.StatusOK,
		Data:   todos,
		Status: "fetch success",
	})
}

func (t *TodoController) GetAllTodo(c *fiber.Ctx) error {
	err, todos := t.TodoService.GetAllTodo()
	if err != nil {
		return err
	}

	return c.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:   fiber.StatusOK,
		Data:   todos,
		Status: "fetch success",
	})
}
