package config

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"github.com/twinj/uuid"
	"go-todos-clean/model"
	"os"
	"strconv"
	"time"
)

type Token struct{}

func NewToken() *Token {
	return &Token{}
}

type TokenInterface interface {
	CreateToken(userid uint64) (*model.TokenDetails, error)
	GetUserFromToken(c *fiber.Ctx) (*model.AccessDetails, error)
	ExtractTokenMetadata(accessToken string) (*model.AccessDetails, error)
}

//Token implements the TokenInterface
var _ TokenInterface = &Token{}

func (t *Token) CreateToken(userid uint64) (*model.TokenDetails, error) {
	td := &model.TokenDetails{}
	td.AtExpires = time.Now().Add(time.Minute * 15).Unix()
	td.TokenUuid = uuid.NewV4().String()

	td.RtExpires = time.Now().Add(time.Hour * 24 * 7).Unix()
	td.RefreshUuid = td.TokenUuid + "++" + strconv.Itoa(int(userid))

	var err error
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["access_uuid"] = td.TokenUuid
	atClaims["user_id"] = userid
	atClaims["exp"] = td.AtExpires
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	td.AccessToken, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return nil, err
	}
	//Creating Refresh Token
	rtClaims := jwt.MapClaims{}
	rtClaims["refresh_uuid"] = td.RefreshUuid
	rtClaims["user_id"] = userid
	rtClaims["exp"] = td.RtExpires
	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	td.RefreshToken, err = rt.SignedString([]byte(os.Getenv("REFRESH_SECRET")))
	if err != nil {
		return nil, err
	}
	return td, nil
}

func TokenValid(accessToken string) error {
	token, err := VerifyToken(accessToken)
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}

func VerifyToken(accessToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(accessToken, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (t *Token) ExtractTokenMetadata(accessToken string) (*model.AccessDetails, error) {
	fmt.Println("WE ENTERED METADATA")
	token, err := VerifyToken(accessToken)
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		accessUuid, ok := claims["access_uuid"].(string)
		if !ok {
			return nil, err
		}
		userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
		if err != nil {
			return nil, err
		}
		return &model.AccessDetails{
			TokenUuid: accessUuid,
			UserId:    userId,
		}, nil
	}
	return nil, err
}

func (t *Token) GetUserFromToken(c *fiber.Ctx) (*model.AccessDetails, error) {
	tokenHeader := c.Get("X-Token-Auth")
	tokenCookies := c.Cookies("accessToken")

	errTokenHeader := TokenValid(tokenHeader)
	if errTokenHeader != nil {
		return nil, errors.New("token invalid")
	}

	errTokenCookies := TokenValid(tokenCookies)
	if errTokenCookies != nil {
		return nil, errors.New("token invalid")
	}

	_, verifyTokenHeader := VerifyToken(tokenHeader)
	if verifyTokenHeader != nil {
		return nil, errors.New("token expired")
	}

	_, verifyTokenCookies := VerifyToken(tokenCookies)
	if verifyTokenCookies != nil {
		return nil, errors.New("token expired")
	}

	tokenDetail, err := t.ExtractTokenMetadata(tokenHeader)
	if err != nil {
		tokenDetail, err := t.ExtractTokenMetadata(tokenCookies)
		if err != nil {
			return nil, errors.New("token invalid")
		}
		return tokenDetail, nil
	}

	return tokenDetail, nil
}
