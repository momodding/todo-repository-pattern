package config

import (
	"github.com/gin-gonic/gin"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"net/http"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}

func CORSConfig() fiber.Handler {
	return cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowCredentials: true,
		AllowHeaders:     "",
		AllowMethods:     "POST, OPTIONS, GET, PUT, DELETE",
		Next:             nil,
	})
}

func AuthFilter() fiber.Handler {
	return func(c *fiber.Ctx) error {
		errTokenHeader := TokenValid(c.Get("X-Token-Auth"))
		errTokenCookies := TokenValid(c.Cookies("accessToken"))
		if errTokenHeader != nil || errTokenCookies != nil {
			return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
				"status": http.StatusUnauthorized,
				"error":  "invalid token",
			})
		}
		return c.Next()
	}
}
