package helper

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"time"
)

type JSONDate time.Time

func (date JSONDate)MarshalJSON() ([]byte, error) {
	//do your serializing here
	stamp := fmt.Sprintf("\"%s\"", time.Time(date).Format("2006-01-02"))
	return []byte(stamp), nil
}

func (date *JSONDate) UnmarshalJSON(b []byte) (err error) {
	s := string(b)

	// Get rid of the quotes "" around the value.
	// A second option would be to include them
	// in the date format string instead, like so below:
	//   time.Parse(`"`+time.RFC3339Nano+`"`, s)
	s = s[1:len(s)-1]

	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		t, err = time.Parse("2006-01-02", s)
	}
	*date = JSONDate(t)
	return
}

func (date *JSONDate) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}

	//result := time.Time{}
	//err := json.Unmarshal(bytes, &result)
	s := string(bytes)
	s = s[1:len(s)-1]

	t, err := time.Parse(time.RFC3339Nano, s)
	if err != nil {
		t, err = time.Parse("2006-01-02", s)
	}
	*date = JSONDate(t)
	return err
}

// Value return json value, implement driver.Valuer interface
func (date JSONDate) Value() (driver.Value, error) {
	return time.Time(date).MarshalJSON()
}


