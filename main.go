package main

import (
	"github.com/gofiber/fiber/v2"
	recover2 "github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
	"go-todos-clean/config"
	"go-todos-clean/repository"
	service2 "go-todos-clean/service"
	"log"
	"os"
)

var DefaultErrorHandler = func(c *fiber.Ctx, err error) error {
	// Default 500 statuscode
	code := fiber.StatusInternalServerError

	if e, ok := err.(*fiber.Error); ok {
		// Override status code if fiber.Error type
		code = e.Code
	}
	// Set Content-Type: text/plain; charset=utf-8
	c.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

	// Return statuscode with error message
	return c.Status(code).JSON(fiber.Map{
		"status":  false,
		"data":    nil,
		"message": err.Error(),
	})
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Println("no env gotten")
	}
}

func main() {
	log.Println(os.Environ())
	dbdriver := os.Getenv("APP_DB_DRIVER")
	host := os.Getenv("APP_DB_HOST")
	password := os.Getenv("APP_DB_PASSWORD")
	user := os.Getenv("APP_DB_USER")
	dbname := os.Getenv("APP_DB_NAME")
	port := os.Getenv("APP_DB_PORT")

	repos, err := repository.NewRepositories(dbdriver, user, password, port, host, dbname)
	if err != nil {
		panic(err)
	}
	defer repos.Close()

	token := config.NewToken()
	service, err := service2.NewServices(repos, token)
	if err != nil {
		panic(err)
	}

	app := fiber.New(fiber.Config{
		ErrorHandler: DefaultErrorHandler,
	})
	app.Use(config.CORSConfig())
	//app.Use(helmet.New())
	app.Use(recover2.New())

	router(app, service, token)

	//Starting the usecase
	appPort := os.Getenv("PORT") //using heroku host
	if appPort == "" {
		appPort = "8888" //localhost
	}
	log.Fatal(app.Listen("0.0.0.0:" + appPort))
}
