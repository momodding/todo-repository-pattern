package userdata

import (
	"go-todos-clean/entity"
	"go-todos-clean/model"
)

type UserDataService interface {
	GetProfile(tokenData *model.AccessDetails) (error, *entity.UserData)

	CreateOrUpdatePersonalProfile(tokenData *model.AccessDetails, request model.ProfileRequest) (error, *entity.UserData)

	CreateOrUpdateSocmed(tokenData *model.AccessDetails, request model.ProfileRequest) (error, *entity.UserData)

	CreateOrUpdateExperience(tokenData *model.AccessDetails, request model.ProfileRequest) (error, *entity.UserData)
}

