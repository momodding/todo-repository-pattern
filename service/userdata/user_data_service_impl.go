package userdata

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/entity"
	"go-todos-clean/model"
	"go-todos-clean/repository/user_data"
)

type userDataServiceImpl struct {
	userDataRepos user_data.UserDataRepository
}

func NewUserData(
	userDataRepos *user_data.UserDataRepository,
) UserDataService {
	return &userDataServiceImpl{
		userDataRepos: *userDataRepos,
	}
}

func (t *userDataServiceImpl) GetProfile(tokenData *model.AccessDetails) (error, *entity.UserData) {
	userData, err := t.userDataRepos.GetByUserId(tokenData.UserId)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, "todo not found"), nil
	}

	return nil, userData
}

func (t *userDataServiceImpl) CreateOrUpdatePersonalProfile(tokenData *model.AccessDetails, request model.ProfileRequest) (error, *entity.UserData) {
	if err := validatePersonalProfileCreation(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil
	}

	userData, err := t.userDataRepos.GetByUserId(tokenData.UserId)
	if err != nil {
		savedUserData, err := t.userDataRepos.Save(&entity.UserData{
			UserId:      tokenData.UserId,
			Fullname:    request.Fullname,
			Address:     request.Address,
			Phonenumber: request.Phonenumber,
			Occupation:  request.Occupation,
			DateOfBirth: request.DateOfBirth,
		})
		if err != nil {
			return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
		}
		return nil, savedUserData
	}

	userData.Fullname = request.Fullname
	userData.Address = request.Address
	userData.Phonenumber = request.Phonenumber
	userData.Occupation = request.Occupation
	userData.DateOfBirth = request.DateOfBirth
	updateUserData, err := t.userDataRepos.Update(userData)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
	}
	return nil, updateUserData
}

func validatePersonalProfileCreation(request model.ProfileRequest) error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Fullname, validation.Required, validation.NilOrNotEmpty),
		validation.Field(&request.Address, validation.Required, validation.NilOrNotEmpty),
		validation.Field(&request.DateOfBirth, validation.Required, validation.NilOrNotEmpty),
		validation.Field(&request.Occupation, validation.Required, validation.NilOrNotEmpty),
		validation.Field(&request.Phonenumber, validation.Required, validation.NilOrNotEmpty),
	)
}

func (t *userDataServiceImpl) CreateOrUpdateSocmed(tokenData *model.AccessDetails, request model.ProfileRequest) (error, *entity.UserData) {
	if err := validateSocmedCreation(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil
	}

	userData, err := t.userDataRepos.GetByUserId(tokenData.UserId)
	if err != nil {
		savedSocmed, err := t.userDataRepos.Save(&entity.UserData{
			UserId: tokenData.UserId,
			Socmed: request.Socmed,
		})
		if err != nil {
			return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
		}
		return nil, savedSocmed
	}

	userData.Socmed = request.Socmed
	updateSocmed, err := t.userDataRepos.Update(userData)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
	}
	return nil, updateSocmed
}

func validateSocmedCreation(request model.ProfileRequest) error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Socmed, validation.Required, validation.NilOrNotEmpty),
	)
}

func (t *userDataServiceImpl) CreateOrUpdateExperience(tokenData *model.AccessDetails, request model.ProfileRequest) (error, *entity.UserData) {
	if err := validateExperienceCreation(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil
	}

	userData, err := t.userDataRepos.GetByUserId(tokenData.UserId)
	if err != nil {
		savedExperience, err := t.userDataRepos.Save(&entity.UserData{
			UserId: tokenData.UserId,
			Experience: request.Experience,
		})
		if err != nil {
			return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
		}
		return nil, savedExperience
	}

	userData.Experience = request.Experience
	updateExperience, err := t.userDataRepos.Update(userData)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
	}
	return nil, updateExperience
}

func validateExperienceCreation(request model.ProfileRequest) error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Experience, validation.Required, validation.NilOrNotEmpty),
	)
}
