package auth

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/config"
	"go-todos-clean/entity"
	"go-todos-clean/model"
	"go-todos-clean/repository/user_credential"
	"go-todos-clean/repository/user_token"
	"time"
)

type authServiceImpl struct {
	userRepos  user_credential.UserCredentialRepository
	tokenRepos user_token.UserTokenRepository
	auth       config.Token
}

func NewAuth(
	userRepos       *user_credential.UserCredentialRepository,
	tokenRepos      *user_token.UserTokenRepository,
	auth *config.Token,
) AuthService {
	return &authServiceImpl{
		userRepos:       *userRepos,
		tokenRepos:      *tokenRepos,
		auth:            *auth,
	}
}

func (t *authServiceImpl) Login(request model.LoginRequest) (error, *entity.User, *model.TokenDetails) {
	if err := validateLogin(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil, nil
	}

	users, err := t.userRepos.GetByEmailOrUsernameWhereActive(request.Username, request.Username)
	if err != nil {
		return fiber.NewError(fiber.StatusUnauthorized, "login gagal"), nil, nil
	}
	if users.Password != request.Password {
		return fiber.NewError(fiber.StatusUnauthorized, "login gagal"), nil, nil
	}

	token, err := t.auth.CreateToken(users.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil, nil
	}

	_, _ = t.tokenRepos.Save(&entity.UserToken{
		User:         users.ID,
		RefreshToken: token.RefreshToken,
		ExpireIn:     time.Now().Add(time.Hour * time.Duration(8)),
	})

	return nil, users, token
}

func (t *authServiceImpl) Register(request model.RegistrationRequest) (error, *entity.User, *model.TokenDetails) {
	if err := validateRegister(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil, nil
	}

	users, err := t.userRepos.GetByEmailOrUsernameWhereActive(request.Email, request.Username)
	if err == nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, "username or email exist"), nil, nil
	}
	if request.Password != request.PasswordConfirm {
		return fiber.NewError(fiber.StatusUnprocessableEntity, "password not match"), nil, nil
	}

	users, err = t.userRepos.Save(&entity.User{
		Username:     request.Username,
		Password:     request.Password,
		Email:        request.Email,
		Role:         request.Role,
		Status:       "Y",
		LoginAttempt: 0,
	})
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil, nil
	}

	token, err := t.auth.CreateToken(users.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil, nil
	}

	_, _ = t.tokenRepos.Save(&entity.UserToken{
		User:         users.ID,
		RefreshToken: token.RefreshToken,
		ExpireIn:     time.Now().Add(time.Hour * time.Duration(8)),
	})

	return nil, users, token
}

func (t *authServiceImpl) RefreshToken(refreshToken string) (error, *entity.UserToken, *model.TokenDetails) {
	tokenData, err := t.tokenRepos.GetByRefreshToken(refreshToken)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, "token tidak ditemukan"), nil, nil
	}

	user, err := t.userRepos.GetById(tokenData.User)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, "user tidak ditemukan"), nil, nil
	}

	token, err := t.auth.CreateToken(user.ID)
	if err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil, nil
	}

	return nil, tokenData, token
}

func validateLogin(request model.LoginRequest) error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Username, validation.Required),
		validation.Field(&request.Password, validation.Required),
	)
}

func validateRegister(request model.RegistrationRequest) error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.Username, validation.Required),
		validation.Field(&request.Email, validation.Required),
		validation.Field(&request.Password, validation.Required),
		validation.Field(&request.PasswordConfirm, validation.Required),
		validation.Field(&request.Role, validation.Required),
	)
}
