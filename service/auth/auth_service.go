package auth

import (
	"go-todos-clean/entity"
	"go-todos-clean/model"
)

type AuthService interface {
	Login(request model.LoginRequest) (error, *entity.User, *model.TokenDetails)
	Register(request model.RegistrationRequest) (error, *entity.User, *model.TokenDetails)
	RefreshToken(refreshToken string) (error, *entity.UserToken, *model.TokenDetails)
}
