package todo

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/entity"
	"go-todos-clean/model"
	"go-todos-clean/repository/user_credential"
	"go-todos-clean/repository/user_todo"
	"net/http"
)

type todoServiceImpl struct {
	todoRepos           user_todo.UserTodoRepository
	userCredentialRepos user_credential.UserCredentialRepository
}

func NewTodos(
	todoRepos *user_todo.UserTodoRepository,
	userCredentialRepos *user_credential.UserCredentialRepository,
) TodoService {
	return &todoServiceImpl{
		todoRepos:           *todoRepos,
		userCredentialRepos: *userCredentialRepos,
	}
}

func (t *todoServiceImpl) SaveTodo(request model.TodoRequest) (error, *entity.UserTodo) {
	if err := validateTodoCreation(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil
	}

	savedTodos, err := t.todoRepos.Save(&entity.UserTodo{
		TaskName:  request.TaskName,
		TaskDesc:  request.TaskDesc,
		UpdatedBy: request.UserId,
	})
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
	}
	return nil, savedTodos
}

func (t *todoServiceImpl) UpdateTodo(id uint64, request model.TodoRequest) (error, *entity.UserTodo) {
	if err := validateTodoCreation(request); err != nil {
		return fiber.NewError(fiber.StatusUnprocessableEntity, err.Error()), nil
	}

	todos, err := t.todoRepos.GetById(id)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, "todo not found"), nil
	}

	todos.TaskName = request.TaskName
	todos.TaskDesc = request.TaskDesc

	todos.UpdatedBy = request.UserId
	updateTodos, err := t.todoRepos.Update(todos)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error()), nil
	}
	return nil, updateTodos
}

func (t *todoServiceImpl) DeleteTodo(id uint64) error {
	_, err := t.todoRepos.GetById(id)
	if err != nil {
		return fiber.NewError(fiber.StatusNotFound, "todo not found")
	}

	err = t.todoRepos.Delete(id)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	return nil
}

func (t *todoServiceImpl) GetTodo(id uint64) (error, *entity.UserTodo) {
	todos, err := t.todoRepos.GetById(id)
	if err != nil {
		return fiber.NewError(http.StatusNotFound, "todo not found"), nil
	}

	return nil, todos
}

func (t *todoServiceImpl) GetMappedTodo(id uint64) (error, *model.TodoResponse) {
	todos, err := t.todoRepos.GetById(id)
	if err != nil {
		return fiber.NewError(http.StatusNotFound, "todo not found"), nil
	}

	users, err := t.userCredentialRepos.GetById(todos.UpdatedBy)
	if err != nil {
		users = &entity.User{Username: "undefined"}
	}

	return nil, &model.TodoResponse{
		TaskName: todos.TaskName,
		TaskDesc: todos.TaskDesc,
		Username: users.Username,
	}
}

func (t *todoServiceImpl) GetAllTodo() (error, []entity.UserTodo) {
	todos, err := t.todoRepos.GetAll()
	if err != nil {
		return fiber.NewError(http.StatusNotFound, "todo not found"), nil
	}

	return nil, todos
}

func validateTodoCreation(request model.TodoRequest) error {
	return validation.ValidateStruct(&request,
		validation.Field(&request.TaskName, validation.Required, validation.NilOrNotEmpty),
		validation.Field(&request.TaskDesc, validation.Required, validation.NilOrNotEmpty),
	)
}
