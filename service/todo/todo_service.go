package todo

import (
	"go-todos-clean/entity"
	"go-todos-clean/model"
)

type TodoService interface {
	SaveTodo(request model.TodoRequest) (error, *entity.UserTodo)

	UpdateTodo(id uint64, request model.TodoRequest) (error, *entity.UserTodo)

	DeleteTodo(id uint64) error

	GetTodo(id uint64) (error, *entity.UserTodo)

	GetMappedTodo(id uint64) (error, *model.TodoResponse)

	GetAllTodo() (error, []entity.UserTodo)
}

