package service

import (
	"go-todos-clean/config"
	"go-todos-clean/repository"
	"go-todos-clean/service/auth"
	"go-todos-clean/service/todo"
	"go-todos-clean/service/userdata"
)

type Services struct {
	Todo     todo.TodoService
	Auth     auth.AuthService
	UserData userdata.UserDataService
}

func NewServices(repos *repository.Repositories, token *config.Token) (*Services, error) {
	return &Services{
		Todo:     todo.NewTodos(&repos.UserTodo, &repos.UserCredential),
		Auth:     auth.NewAuth(&repos.UserCredential, &repos.UserToken, token),
		UserData: userdata.NewUserData(&repos.UserData),
	}, nil
}
