package main

import (
	"github.com/gofiber/fiber/v2"
	"go-todos-clean/config"
	"go-todos-clean/controller"
	"go-todos-clean/service"
)

func router(r *fiber.App, service *service.Services, token *config.Token) {
	todosHandler := controller.NewTodoController(&service.Todo, token)
	authHandler := controller.NewAuthController(&service.Auth)
	userDataHandler := controller.NewUserDataController(&service.UserData, token)

	r.Get("/", func(c *fiber.Ctx) error {
        return c.SendString("Hello, World 👋!")
    })

	v1 := r.Group("/v1")

	v1.Post("/auth/login", authHandler.Login)
	v1.Get("/auth/token", authHandler.RefreshToken)
	v1.Post("/auth/register", authHandler.Register)
	v1.Post("/auth/logout", authHandler.Logout)

	v1.Post("/todo", config.AuthFilter(), todosHandler.SaveTodo)
	v1.Put("/todo/:todo_id", config.AuthFilter(), todosHandler.UpdateTodo)
	v1.Get("/todo/:todo_id", config.AuthFilter(), todosHandler.GetTodo)
	v1.Get("/todo/:todo_id/mapped", config.AuthFilter(), todosHandler.GetMappedTodo)
	v1.Delete("/todo/:todo_id", config.AuthFilter(), todosHandler.DeleteTodo)
	v1.Get("/todo", config.AuthFilter(), todosHandler.GetAllTodo)

	v1.Get("/profile", config.AuthFilter(), userDataHandler.GetProfile)
	v1.Post("/profile/personal", config.AuthFilter(), userDataHandler.CreateOrUpdatePersonalProfile)
	v1.Post("/profile/socmed", config.AuthFilter(), userDataHandler.CreateOrUpdateSocmed)
	v1.Post("/profile/experience", config.AuthFilter(), userDataHandler.CreateOrUpdateExperience)
}
