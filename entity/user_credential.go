package entity

import (
	"html"
	"strings"
	"time"
)

type User struct {
	ID           uint64    `gorm:"column:uc_id;primary_key;auto_increment" json:"id"`
	Username     string    `gorm:"column:uc_username;varchar:100;not null;unique" json:"username"`
	Password     string    `gorm:"column:uc_password;varchar:100;not null"`
	Email        string    `gorm:"column:uc_email;varchar:100;not null" json:"email"`
	Status       string    `gorm:"column:uc_status;enum('Y','N')" json:"status"`
	Role         uint64    `gorm:"column:uc_role"`
	LoginAttempt uint64    `gorm:"column:uc_login_attempt"`
	CreatedAt    time.Time `gorm:"column:uc_created_at;autoCreateTime"`
	UpdatedAt    time.Time `gorm:"column:uc_updated_at;autoUpdateTime"`
}

type UserCredentialTable interface {
	TableName() string
}

func (User) TableName() string {
	return "user_credential"
}

func (entity *User) BeforeSave() {
	entity.Username = html.EscapeString(strings.TrimSpace(entity.Username))
	entity.Email = html.EscapeString(strings.TrimSpace(entity.Email))
	entity.Password = html.EscapeString(strings.TrimSpace(entity.Password))
}

func (entity *User) Prepare() {
	entity.Username = html.EscapeString(strings.TrimSpace(entity.Username))
	entity.Email = html.EscapeString(strings.TrimSpace(entity.Email))
	entity.Password = html.EscapeString(strings.TrimSpace(entity.Password))
}
