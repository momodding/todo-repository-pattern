package entity

import (
	dataType "go-todos-clean/helper"
	"html"
	"strings"
	"time"
)

type UserData struct {
	ID          uint64        `gorm:"column:ud_id;primary_key;auto_increment" json:"-"`
	UserId      uint64        `gorm:"column:ud_uc_id" json:"userId"`
	Fullname    string        `gorm:"column:ud_fullname;size:25" json:"fullname"`
	Address     string        `gorm:"column:ud_address;size:100" json:"address"`
	DateOfBirth dataType.JSONDate `gorm:"column:ud_dob" json:"dateOfBirth"`
	Occupation  string        `gorm:"column:ud_occupation;size:25" json:"occupation"`
	Phonenumber string        `gorm:"column:ud_phone;size:15" json:"phonenumber"`
	Socmed      string        `gorm:"column:ud_socmed" json:"socmed"`
	Experience  string        `gorm:"column:ud_experience" json:"experience"`
	CreatedAt   *time.Time    `gorm:"column:ud_created_at;autoCreateTime" json:"-"`
	UpdatedAt   *time.Time    `gorm:"column:ud_updated_at;autoUpdateTime" json:"-"`
}

type UserDataTable interface {
	TableName() string
}

func (UserData) TableName() string {
	return "userdata"
}

func (entity *UserData) BeforeSave() {
	entity.Fullname = html.EscapeString(strings.TrimSpace(entity.Fullname))
	entity.Address = html.EscapeString(strings.TrimSpace(entity.Address))
	entity.Occupation = html.EscapeString(strings.TrimSpace(entity.Occupation))
	entity.Phonenumber = html.EscapeString(strings.TrimSpace(entity.Phonenumber))
}

func (entity *UserData) Prepare() {
	entity.Fullname = html.EscapeString(strings.TrimSpace(entity.Fullname))
	entity.Address = html.EscapeString(strings.TrimSpace(entity.Address))
	entity.Occupation = html.EscapeString(strings.TrimSpace(entity.Occupation))
	entity.Phonenumber = html.EscapeString(strings.TrimSpace(entity.Phonenumber))
}
