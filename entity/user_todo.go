package entity

import (
	"html"
	"strings"
	"time"
)

type UserTodo struct {
	ID        uint64     `gorm:"column:ut_id;primary_key;auto_increment" json:"id"`
	TaskName  string     `gorm:"column:ut_task_name;size:100;not null" json:"name"`
	TaskDesc  string     `gorm:"column:ut_task_description;text;not null;unique" json:"description"`
	CreatedAt *time.Time `gorm:"column:ut_created_at;autoCreateTime" json:"-"`
	UpdatedAt *time.Time `gorm:"column:ut_updated_at;autoUpdateTime" json:"-"`
	DeletedAt *time.Time `gorm:"column:ut_deleted_at" json:"-"`
	UpdatedBy uint64     `gorm:"column:ut_uc_id" json:"userId"`
}

type UserTodoTable interface {
	TableName() string
}

func (UserTodo) TableName() string {
	return "user_todo"
}

func (entity *UserTodo) BeforeSave() {
	entity.TaskName = html.EscapeString(strings.TrimSpace(entity.TaskName))
	entity.TaskDesc = html.EscapeString(strings.TrimSpace(entity.TaskDesc))
}

func (entity *UserTodo) Prepare() {
	entity.TaskName = html.EscapeString(strings.TrimSpace(entity.TaskName))
	entity.TaskDesc = html.EscapeString(strings.TrimSpace(entity.TaskDesc))
}
