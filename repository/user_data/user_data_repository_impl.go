package user_data

import (
	"errors"
	"github.com/jinzhu/gorm"
	"go-todos-clean/entity"
)

type UserDataRepo struct {
	db *gorm.DB
}

func NewUserDataRepository(db *gorm.DB) *UserDataRepo {
	return &UserDataRepo{db}
}

var _ UserDataRepository = &UserDataRepo{}

func (r *UserDataRepo) Save(todos *entity.UserData) (*entity.UserData, error) {
	err := r.db.Debug().Create(&todos).Error
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (r *UserDataRepo) Update(todos *entity.UserData) (*entity.UserData, error) {
	err := r.db.Debug().Save(&todos).Error
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (r *UserDataRepo) Delete(u uint64) error {
	var todos entity.UserData
	err := r.db.Debug().Where("ud_id = ?", u).Delete(&todos).Error
	if err != nil {
		return errors.New("database error, please try again")
	}
	return nil
}

func (r *UserDataRepo) GetById(u uint64) (*entity.UserData, error) {
	var todos entity.UserData
	err := r.db.Debug().Where("ud_id = ?", u).Take(&todos).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("food not found")
	}
	return &todos, nil
}

func (r *UserDataRepo) GetByUserId(u uint64) (*entity.UserData, error) {
	var todos entity.UserData
	err := r.db.Debug().Where("ud_uc_id = ?", u).Take(&todos).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("food not found")
	}
	return &todos, nil
}
