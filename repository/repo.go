package repository

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"go-todos-clean/repository/user_credential"
	"go-todos-clean/repository/user_data"
	"go-todos-clean/repository/user_todo"
	"go-todos-clean/repository/user_token"
)

type Repositories struct {
	UserTodo       user_todo.UserTodoRepository
	UserCredential user_credential.UserCredentialRepository
	UserToken      user_token.UserTokenRepository
	UserData       user_data.UserDataRepository
	db             *gorm.DB
}

func NewRepositories(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*Repositories, error) {
	//DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
	db, err := gorm.Open(Dbdriver, DBURL)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)

	return &Repositories{
		UserTodo:       user_todo.NewUserTodoRepository(db),
		UserCredential: user_credential.NewUserCredentialRepository(db),
		UserToken:      user_token.NewUserTokenRepository(db),
		UserData:       user_data.NewUserDataRepository(db),
		db:             db,
	}, nil
}

//closes the  database connection
func (s *Repositories) Close() error {
	return s.db.Close()
}
