package user_credential

import (
	"errors"
	"github.com/jinzhu/gorm"
	"go-todos-clean/entity"
)

type UserCredentialRepo struct {
	db *gorm.DB
}

func NewUserCredentialRepository(db *gorm.DB) *UserCredentialRepo {
	return &UserCredentialRepo{db}
}

var _ UserCredentialRepository = &UserCredentialRepo{}

func (r *UserCredentialRepo) Save(user *entity.User) (*entity.User, error) {
	err := r.db.Debug().Create(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (r *UserCredentialRepo) Update(user *entity.User) (*entity.User, error) {
	err := r.db.Debug().Save(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (r *UserCredentialRepo) Delete(id uint64) error {
	var user entity.User
	err := r.db.Debug().Where("uc_id = ?", id).Delete(&user).Error
	if err != nil {
		return errors.New("database error, please try again")
	}
	return nil
}

func (r *UserCredentialRepo) GetById(id uint64) (*entity.User, error) {
	var user entity.User
	err := r.db.Debug().Where("uc_id = ?", id).Take(&user).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("user not found")
	}
	return &user, nil
}

func (r *UserCredentialRepo) GetByEmailOrUsernameWhereActive(email string, username string) (*entity.User, error) {
	var user entity.User
	err := r.db.Debug().Where("uc_username = ? or uc_email = ?", username, email).Take(&user).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("user not found")
	}
	return &user, nil
}

func (r *UserCredentialRepo) GetAll() ([]entity.User, error) {
	var users []entity.User
	err := r.db.Debug().Limit(100).Order("uc_created_at desc").Find(&users).Error
	if err != nil {
		return nil, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("user not found")
	}
	return users, nil
}
