package user_credential

import "go-todos-clean/entity"

type UserCredentialRepository interface {
	Save(*entity.User) (*entity.User, error)
	Update(*entity.User) (*entity.User, error)
	Delete(uint64) error
	GetById(uint64) (*entity.User, error)
	GetByEmailOrUsernameWhereActive(email string, username string) (*entity.User, error)
	GetAll() ([]entity.User, error)
}
