package user_todo

import (
	"errors"
	"github.com/jinzhu/gorm"
	"go-todos-clean/entity"
)

type UserTodoRepo struct {
	db *gorm.DB
}

func NewUserTodoRepository(db *gorm.DB) *UserTodoRepo {
	return &UserTodoRepo{db}
}

var _ UserTodoRepository = &UserTodoRepo{}

func (r *UserTodoRepo) Save(todos *entity.UserTodo) (*entity.UserTodo, error) {
	err := r.db.Debug().Create(&todos).Error
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (r *UserTodoRepo) Update(todos *entity.UserTodo) (*entity.UserTodo, error) {
	err := r.db.Debug().Save(&todos).Error
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (r *UserTodoRepo) Delete(u uint64) error {
	var todos entity.UserTodo
	err := r.db.Debug().Where("ut_id = ?", u).Delete(&todos).Error
	if err != nil {
		return errors.New("database error, please try again")
	}
	return nil
}

func (r *UserTodoRepo) GetById(u uint64) (*entity.UserTodo, error) {
	var todos entity.UserTodo
	err := r.db.Debug().Where("ut_id = ?", u).Take(&todos).Error
	if err != nil {
		return nil, errors.New("database error, please try again")
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("food not found")
	}
	return &todos, nil
}

func (r *UserTodoRepo) GetAll() ([]entity.UserTodo, error) {
	var todos []entity.UserTodo
	err := r.db.Debug().Limit(100).Order("ut_created_at asc").Find(&todos).Error
	if err != nil {
		return nil, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("user not found")
	}
	return todos, nil
}
